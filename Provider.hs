module Provider where
import Text.Printf
class MapProvider a where
	name 	:: a -> String
	minZoom	:: a -> Integer
	maxZoom	:: a -> Integer
	tileLen :: a -> Integer
	tileUrl	:: a -> Integer -> Integer -> Integer -> String

data Osm = Osm
instance MapProvider Osm where
	name 	_ 		= "Open Street Map"
	minZoom _ 		= 0
	maxZoom _ 		= 18
	tileLen _ 		= 256
	tileUrl _ z x y = 
		printf "http://c.tile.openstreetmap.org/%d/%d/%d.png" z x y