module Geometry where
import Various 
import Interval
import Control.Arrow


type Point		= (Double, Double)
type PageDims 	= (Double, Double)
data Box		= Box (Interval Double) (Interval Double) 
instance Show Box where
	show (Box h v) = show h ++ "x" ++ show v

-- Determine whether a box (which is rotated if needed) is smaller
-- than the given maximum dimensions.
fits_into :: Box -> PageDims -> Bool
fits_into (Box x y) (pw, ph) =
	let	w = width x
		h = width y
	in	(w <= pw && h <= ph) || (w <= ph && h <= pw)

-- determine whether a set of points fits into the given dimensions
-- with respect to a map scale and the latitude of the points.
fits_scale_page :: Double -> PageDims -> [Point] -> Bool
fits_scale_page s (pw, ph) ps =
	let	s'	= localScale ps s
	in	enclose ps `fits_into` (pw*s', ph*s')

-- compute the zoom level that satisfies a given resolution and scale.
zoomLevel      ::	Double -> Double -> Integer
zoomLevel r s  =	round $ min 18 $ max 0 $ 22.55519 + logBase 2 (r/s)

-- given a normal scale ratio (e.g. 10,000 in 1:10,000), localScale
-- gives the corresponding scale in map units. The conversion is
-- dependend of the actual latitude and hence localScale takes a
-- list of Points in order to use their average latitude.
localScale       ::	 [Point] -> Double -> Double
localScale ps s  =	 s / 40074000 / (cos $ gd $ pi * (1 - 2 * (mean $ map snd ps)))

-- for a box that is smaller than the dimensions, grow up the box
-- until it has the given dimensions while keeping the orientation
-- (horizontal or vertical) and the center of the box.
enlargeInto :: Box -> PageDims -> Box
enlargeInto (Box x y) (pw, ph) =
	let	cx = center x
		cy = center y
		(w,h) = if s && s' || not s && not s'
			then (pw, ph)
			else (ph, pw)
		s  = width x < width y
		s' = pw < ph
	in	Box ((cx-w/2) `To` (cx+w/2)) ((cy-h/2) `To` (cy+h/2))


enclose :: [Point] -> Box
enclose ps =
	let	h = hull $ fst `map` ps
		v = hull $ snd `map` ps
	in	Box h v

	
encloseInPage   :: Double -> PageDims -> [Point] -> Box
encloseInPage s (pw, ph) ps =
	let	s'	=  localScale ps s
	in	enclose ps `enlargeInto` (pw*s', ph*s')


local :: Box -> Point -> Point
local (Box h v) = subtract (inf h) *** subtract (inf v)

