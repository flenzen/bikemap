module IO where
import Various
import Geometry
import Interval
import Provider

import Control.Monad
import Control.Arrow
import Text.Printf
import Text.XML.HXT.Core
import Text.XML.HXT.XPath
import Data.Tree.NTree.TypeDefs


-- reads in an XmlTree that represents gpx data
read_gpx :: XmlTree ->[[Point]]
read_gpx gpx =
	let	-- trks holds a list of all tracks (or alternatively routes).
		-- for each track, make a list of lats and lons and retrieve
		-- a list of lists of coordinates
		trks 	= getXPath "//trk | //rte" gpx
		lats	= getXPath "//trkpt/@lat/text() | //rtept/@lat/text()" `map` trks
		lons	= getXPath "//trkpt/@lon/text() | //rtept/@lon/text()" `map` trks
		-- convert a text node into a double value
		t2d		= \(NTree (XText s) _) -> read s
	in	-- zip the lists together and convert the coordinates
		zipWith (map (deg2lon . t2d *** deg2lat . t2d) ... zip) lons lats


-- given a zoom level, a box, a list of points in the box and an index,
-- osm_magick downloads the box with the zoom level given, draws the
-- points as a route into the box, and saves the image into a file with
-- the index in its filename.
osm_magick :: Integer -> Box -> [Point] -> Integer -> String
osm_magick z b@(Box h v) ps n =
	let	-- in future, other maps may also be used
		provider		= Osm
		-- get the map tiles that need to be downloaded. 
		(l, ths, _) 	= tiles (2^z :: Integer) h
		(t, tvs, _) 	= tiles (2^z :: Integer) v
		ts				= ths `dyad` tvs
		-- the size of the whole world map in pixels.
		px				= 256 * 2^z
		-- The temporary file name for a tile.
		tmpName x y = printf "%%TEMP%%\\%d_%d.png" x y :: String
		
		--compose the command call strings
		wget_str = 
			let	xyUrl	= uncurry $ tileUrl provider z
				xyFile	= uncurry tmpName
				makeCmd	= uncurry $ printf "wget %s -qO%s\n"
			in join $ map (makeCmd <<< xyUrl &&& xyFile) $ join ts
		
		files_str = 
			let	xyUrl	= uncurry $ tileUrl provider z
				xyFile	= uncurry tmpName
				spFold  = \s t -> s ++ " " ++ t
				column c= foldl spFold "(" (map xyFile c) ++ " -append ) "
			in "( " ++ (join $ map column $ ts) ++ ") +append "
		
		crop_str =
			let	w_px 	= round $ width h * px :: Integer
				h_px 	= round $ width v * px :: Integer
				l_px 	= round $ l * px :: Integer
				t_px 	= round $ t * px :: Integer
			in	(printf "-crop %dx%d+%d+%d " w_px h_px l_px t_px) :: String
		
		path_str = 
			let	toPx	= show . round . (px*)
				coord	= uncurry (printf "%s,%s ") . liftP toPx . local b
				cStr	= foldl1 (++) $ map coord ps
			in	if null ps
				then "" 
				else "-fill none -stroke red -strokewidth 3 -draw \"polyline " ++ cStr ++ "\" "
		
		-- arrange all the command Strings into a single call to Magick
		cmd				= wget_str ++ (printf "convert %s%s%sout_%d.png\n" files_str crop_str path_str n) :: String
	in cmd