.PHONY: clean

bikemap: $(wildcard *.hs)
	ghc -o bikemap Main

test: bikemap
	./bikemap -iroute.gpx -s5000 -r70 | sed -e"s/(/\\\\(/g" -e"s/)/\\\\)/g" -e"s/%TEMP%/\\/tmp\\//g" | bash

clean: 
	rm *.o *.hi *.png
