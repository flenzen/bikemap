module Options where
import Geometry
import System.Console.GetOpt

-- this structure holds all available cmd line options.
data Options = Options{
	optInput	:: IO String,
	optPageDims :: PageDims,
	optScale	:: Double,
	optRes		:: Double,
	optExecute	:: Bool
}

-- the default options to use when no command line argument is given.
defaultOptions = Options{
	optInput	= getContents,
	optPageDims	= (0.210, 0.297),
	optScale	= 10000,
	optRes		= 300,
	optExecute	= False
}

-- how shall each command line argument alter an options record?
options :: [ OptDescr (Options -> Options) ]
options = [
	Option "i"	["input"]		(ReqArg (\a o-> o{optInput = readFile a } ) "FILE")									"input file",
	Option "s" 	["scale"]		(ReqArg (\a o-> o{optScale = read a } ) "NUMBER")									"scale, as n in 1:n",
	Option "r" 	["resolution"]	(ReqArg (\a o-> o{optRes = read a } ) "NUMBER")									"resolution in dpi",
	Option "h"	["height"]		(ReqArg (\a o-> o{optPageDims=(fst $ optPageDims o, read a / 1000) } ) "LENGTH")	"paper height in mm",
	Option "w"	["width"]		(ReqArg (\a o-> o{optPageDims=(read a / 1000, snd $ optPageDims o) } ) "LENGTH")	"paper width in mm",
	Option "p"	["paper"]		(ReqArg (\a o-> o{optPageDims=paperFormatDims a } ) "FORMAT")						"one of the following: a4, a5, a6",
	Option "e"	["execute"]		(NoArg	(\o  -> o{optExecute=True}))												"immediately execute"
	]

paperFormatDims s = 
	case s of
		"a4" -> (0.210, 0.297)
		"a5" -> (0.148, 0.210)
		"a6" -> (0.105, 0.148)