module Interval where

-- holds an interval of values, e.g. the interval 1 `To` 3
data Interval a = To a a
instance Show a => Show (Interval a) where
	show (x `To` y) = "[" ++ show x ++ "," ++ show y ++ "]"

center :: Fractional a => Interval a -> a
center (l`To`r) = (l+r) / 2

width :: Num a => Interval a -> a
width (l`To`r) = r-l

sup (l`To`r) = r
inf (l`To`r) = l

hull :: Ord a => [a] -> Interval a
hull xs = (minimum xs) `To` (maximum xs)

-- tiles t i divides the unit interval into t equally sized parts
-- and gives a triple (l, ts, r) where ts is a list containing the
-- indices t (partially) touches, and l and r denominate the distance
-- between the left-/rightmost subcell and t.
tiles :: RealFrac a => Integral b => b -> Interval a -> (a, [b], a)
tiles t i =
	let
		fI = fromIntegral
		d = fI t
		l = floor $ (inf i) * d
		r = floor $ (sup i) * d
		rl =  (inf i) - (fI $ floor   $ (inf i) * d)/d
		rr = -(sup i) + (fI $ ceiling $ (sup i) * d)/d
	in
		(rl, [l..r], rr)