module Various
	where
import Control.Arrow

-- groupl p l divides the list l into a list of lists in such a way
-- that every of these lists suffises the predicate p. groupl starts
-- grouping from the left end of l.
groupl :: ([a] -> Bool) -> [a] -> [[a]]
groupl _ [] = []
groupl c xs =
	gs : groupl c rs
	where
		groupl' :: ([a] -> Bool) -> [a] -> [a] -> ([a],[a])
		groupl' _ gs [] 	= (gs, [])
		groupl' c gs (x:xs) = 
			if c (gs ++ [x])
				then groupl' c (gs ++ [x]) xs 
				else (gs, x:xs)
		(gs, rs) = groupl' c [] xs

-- (...) is a replacement for the function composition operator (.)
-- for the case that the second operand (the function that is executed
-- first) takes to arguments.
infixr 9 ...
(...) :: (c->d) -> (a->b->c) -> (a->b->d)
(...) g f x y = g $ f x y

-- dyadWith f xs ys computes the dyadic product of the lists xs and ys,
-- i.e. a matrix where the (n,k)-cell contains the value of f applied
-- to the n-th element of xs and the k-th element of ys. The third operand
-- iterates the inner lists.
dyadWith :: (a->b->c) -> [a] -> [b] -> [[c]]
dyadWith f xs ys = map (\x -> map (f x) ys) xs

-- dyad xs ys simply produces a matrix of pairs of the elements of xs
-- and ys. The second operand iterates the inner lists.
dyad = dyadWith (,)

gd  = atan . sinh
agd = atanh . sin
-- conversion functions between degree, radians and map coordinates, where
-- 0,0 is 85°N, 180°O, and numbers are increasing to south west til 1,1.
deg2lat l = (pi - (agd $ deg2rad l)) / (2*pi)
deg2lon	l = (180+l)/360
lat2deg l = rad2deg $ gd $ pi * (1 - 2 * l)
lon2deg l = 360*l - 180
deg2rad = (pi/180 *)
rad2deg = (180/pi *)
mean xs = sum xs / fromIntegral (length xs)

-- lift a function on values a to a function on pairs of as by applying
-- the function on both pair elements (***-operator)
liftP :: (a -> b) -> (a,a) -> (b,b)
liftP a = a *** a

