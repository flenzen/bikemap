module Main where
import Options
import Geometry
import IO
import Various

import System.Environment
import System.Exit
import System.IO
import System.Process
import Control.Monad
import System.Console.GetOpt
import Text.XML.HXT.Core hiding (when)

main = do
	-- read inthe command line args and give an error message if
	-- something went wrong
	args <- getArgs
	let (acs, n, es)= getOpt RequireOrder options args
	let	os 			= (foldl (.) id acs) defaultOptions
	when (not $ null es) $
		putStr ("The following error(s) occured:\n" ++ concat (map (++ "\n") es) ++ usageInfo "Use as following:" options)
		>> exitFailure
	
	-- Read and interprete the gpx file contents into list of lists of
	-- points, i.e. a list of routes.
	gpx <- read_gpx `fmap` head `fmap` (optInput os >>= runX . readString [])
	
	-- group the first route by the available page size, and number the
	-- groups into ned_groups
	let grouper 	= fits_scale_page (optScale os) (optPageDims os)
	let groups 		= grouper `groupl` head gpx
	let ned_groups 	= [1..] `zip` groups
	
	-- create the bash-commands to download the pages.
	let downloader (n, ps) =
		osm_magick											
			(zoomLevel (optRes os) (optScale os))
			(encloseInPage (optScale os) (optPageDims os) ps) 
			(head gpx)
			n
	let cmd = foldl1 (++) $ downloader `map` ned_groups :: String
	
	if (optExecute os)
	then readProcess "cmd" [] cmd >>= putStrLn
	else putStrLn cmd